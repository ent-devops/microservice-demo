from flask import Flask, render_template, request
from wtforms import Form, TextField
import requests

# App config.
DEBUG = True
app = Flask(__name__, template_folder='/src')
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
api_gateway = 'http://api'


class AppForm(Form):
    pass


class GeocodeForm(Form):
    search_center = TextField('search_center:')
    query = TextField('query:')
    api_key = TextField('api_key:')


class ReverseGeocodeForm(Form):
    coordinate = TextField('coordinate:')
    api_key = TextField('api_key:')


@app.route("/", methods=['GET', 'POST'])
def hello():
    form = AppForm(request.form)
    geocodeform = GeocodeForm(request.form)
    reversegeocodeform = ReverseGeocodeForm(request.form)
    if request.method == 'POST':
        if request.form['submit'] == 'Geocode':
            return render_template('geocode.html', form=geocodeform)
        else:
            return render_template('reversegeocode.html',
                                   form=reversegeocodeform)
    return render_template('index.html', form=form)


@app.route("/geocode", methods=['GET', 'POST'])
def geocode():
    form = GeocodeForm(request.form)
    gc_result = 'No result'

    if request.method == 'POST':
        search_center = request.form['search_center']
        query = request.form['query']
        headers = {'X-API-Key': request.form['api_key']}
        payload = {'query': query, 'search_center': search_center}
        gc_result = requests.get(api_gateway + '/geocode',
                                 params=payload, headers=headers)

    return render_template('geocode.html',
                           form=form,
                           result=gc_result.text)


@app.route("/reversegeocode", methods=['GET', 'POST'])
def reversegeocode():
    form = ReverseGeocodeForm(request.form)
    gc_result = 'No result'

    if request.method == 'POST':
        coordinate = request.form['coordinate']
        headers = {'X-API-Key': request.form['api_key']}
        payload = {'coordinate': coordinate}
        gc_result = requests.get(api_gateway + '/reversegeocode',
                                 params=payload, headers=headers)

    return render_template('reversegeocode.html',
                           form=form,
                           result=gc_result.text)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
