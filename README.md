DevOps Techinical Evaluation
============================

Test Summary:
- Setup or use an existing AWS acccount (take advantage of the free tier)
- Setup a 6 node Docker Swarm (3 Manager nodes + 3 Worker nodes)
- Setup a Continous Integration pipeline for all the services from the
- Setup a Continous Delivery pipeline for deploying the microservices to the Docker Swarm
- Provide URL to the API gateway to show that the web app works and returns results
- API Key for the web app will be provided.

Detailed Instructions:
This is a test to determine your understanding of microservice architecture, CI/CD frameworks, container technologies and operations. There is no right way to complete this evaluation. We mainly want to focus on your fundamentals and creativity in coming with the solution. We do not expect a production grade solution and understand that the given amount of time may not be sufficient to fully implement the solution.

Please clone the https://gitlab.com/ent-devops/microservice-demo to your gitlab account (please create a free account if you don't have already).

The first ask is to setup a 6 node Docker Swarm cluster. You may pick one of the many available methods to set this up in your AWS account.

After the swarm is up, create a CI pipeline to continously build docker images and upload them to the docker registry. Incorporate and highlight CI best practices (within reason considering the given time).

Setup a CD pipeline to deploy the app to docker swarm.

After the completion of the project please send an email to Scott.Benjamin@comtechtel.com and Apoorva.Kulkarni@comtechtel.com by EOD and provide access to:
  - URLs to access the app
  - Gitlab repo where you have cloned the project

Highlight any features or deployment strategies that you take advantage of.

Helpful Links
-------------
https://docs.docker.com/

https://hub.docker.com

https://docs.docker.com/docker-for-aws/

https://docs.docker.com/engine/swarm/

https://about.gitlab.com/features/gitlab-ci-cd/

https://docs.gitlab.com/ce/ci/quick_start/README.html

https://docs.gitlab.com/runner/install/docker.html

https://gitlab.com/users/sign_in

https://aws.amazon.com/
